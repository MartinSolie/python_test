from lxml import html
import requests
from currency import Currency

def get_currencies_list():
    """Parses site and returns list of currency objects"""
    SITE_URL = "http://coinmarketcap.com/all/views/all/"
    page = requests.get(SITE_URL)
    tree = html.fromstring(page.content)

    names = tree.xpath('//table [@id="currencies-all"]/tbody/tr/td[contains(@class, "currency-name")]/a/text()')
    symbols = tree.xpath('//table [@id="currencies-all"]/tbody/tr/td[@class = "text-left"]/text()')
    market_caps = tree.xpath('//table [@id="currencies-all"]/tbody/tr/td[contains(@class, "market-cap")]/@data-usd')
    prices = tree.xpath('//table [@id="currencies-all"]/tbody/tr/td/a[@class ="price"]/@data-usd')
    volumes = tree.xpath('//table [@id="currencies-all"]/tbody/tr/td/a[@class = "volume"]/@data-usd')

    #if there is no data about these properties - there is no proper @class attribute of the element, so I don't know
    #how to scratch them (
    #supplies = tree.xpath('//table [@id="currencies-all"]/tbody/tr/td/a/text()')
    #percents_1h = tree.xpath('//table [@id="currencies-all"]/tbody/tr/td[contains(@class, "percent-1h")]/@data-usd')
    #percents_24h = tree.xpath('//table [@id="currencies-all"]/tbody/tr/td[contains(@class, "percent-24h")]/@data-usd')
    #percents_7d = tree.xpath('//table [@id="currencies-all"]/tbody/tr/td[contains(@class, "percent-7d")]/@data-usd')

    currencies = []

    for index in xrange(0, len(names)):
        currencies.append(Currency(names[index], symbols[index], market_caps[index], prices[index], volumes[index]))

    return currencies

def get_json_string(currencies):
    """Creates some kind of json string"""

    jsn = '{currencies:[\n'

    for curr in currencies:
        jsn += '\t%s,\n' % curr.to_json()

    jsn = jsn[:len(jsn) - 2]
    jsn += '\n]}'

    return jsn

def write_data_to_file(data, file_name):
    f = open(file_name, "w")
    f.write(data)
    f.close()

currencies = get_currencies_list()

jsn = get_json_string(currencies)

print len(jsn)

#write_data_to_file(jsn, "data.json")