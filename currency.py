class Currency(object):
    def __init__(self, name, symbol, market_cap, price, volume):
        self.name = name
        self.symbol = symbol
        self.market_cap = market_cap
        self.price = price
        #self.supply = supply
        self.volume = volume

    def __repr__(self):
        result = "Name: %s (%s)\n" \
                 "Market cap: %s\n" \
                 "Price: $%s\n" \
                 "Volume: $%s\n" % (self.name, self.symbol, self.market_cap, self.price, self.volume)
        return result

    def to_json(self):
        return {"name" : str(self.name), "symbol" : str(self.symbol), "market_cap" : str(self.market_cap),
                "price" : str(self.price), "volume" : str(self.volume)}